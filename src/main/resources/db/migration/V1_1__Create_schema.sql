
CREATE TABLE IF NOT EXISTS users (
  id            BIGSERIAL     PRIMARY KEY,
  username      VARCHAR(60)   NOT NULL,
  password      VARCHAR(60)   NOT NULL,
  enabled       BOOLEAN       DEFAULT NULL,
  first_name    VARCHAR(45)   DEFAULT NULL,
  last_name     VARCHAR(45)   DEFAULT NULL,
  middle_name   VARCHAR(45)   DEFAULT NULL
);

CREATE TABLE IF NOT EXISTS roles (
  id         BIGSERIAL    PRIMARY KEY,
  user_id    BIGINT       DEFAULT NULL    REFERENCES users (id) ON DELETE CASCADE ON UPDATE CASCADE,
  role_name  VARCHAR(60)  DEFAULT NULL
);