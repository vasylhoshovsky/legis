/*
 * Copyright (C) VHSoft, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Vasyl Hoshovskyi <vasyl.hoshovskyi@vhsoft.com.ua>
 */



package ua.com.vhsoft.legis.domain.user.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import ua.com.vhsoft.legis.domain.user.User;

public interface UserRepository extends JpaRepository<User, Long> {
    User findOneByUsername(String username);
}
