/*
 * Copyright (C) VHSoft, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Vasyl Hoshovskyi <vasyl.hoshovskyi@vhsoft.com.ua>
 */

package ua.com.vhsoft.legis.domain.user.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import ua.com.vhsoft.legis.domain.user.User;
import ua.com.vhsoft.legis.domain.user.dto.UserDto;
import ua.com.vhsoft.legis.domain.user.service.UserService;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @Author Vasyl Hoshovskyi <vasyl.hoshovskyi at vhsoft.com.ua>
 */
@RestController
public class UserController {

    @Autowired
    private ConversionService conversionService;
    @Autowired
    private UserService userService;

    @GetMapping("/users/{id}")
    public UserDto getUser(@PathVariable("id") String id) {
        User user = userService.findById(Long.valueOf(id));
        return conversionService.convert(user, UserDto.class);
    }

    @GetMapping("/users")
    public List<UserDto> getUsers() {
        List<User> users = userService.findAll();
        return users.stream()
                .map(u-> conversionService.convert(u, UserDto.class))
                .collect(Collectors.toList());
    }

}
