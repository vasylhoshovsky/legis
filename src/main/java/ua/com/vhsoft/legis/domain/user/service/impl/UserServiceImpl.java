/*
 * Copyright (C) VHSoft, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Vasyl Hoshovskyi <vasyl.hoshovskyi@vhsoft.com.ua>
 */



package ua.com.vhsoft.legis.domain.user.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import ua.com.vhsoft.legis.domain.user.User;
import ua.com.vhsoft.legis.domain.user.dao.UserRepository;
import ua.com.vhsoft.legis.domain.user.service.UserService;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return userRepository.findOneByUsername(username);
    }

    @Override
    public User findById(Long id) {
        Assert.notNull(id, "User id should not be null");
        return userRepository.findOne(id);
    }

    @Override
    public List<User> findAll() {
        return userRepository.findAll();
    }
}
