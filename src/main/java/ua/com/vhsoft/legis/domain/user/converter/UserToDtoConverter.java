/*
 * Copyright (C) VHSoft, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Vasyl Hoshovskyi <vasyl.hoshovskyi@vhsoft.com.ua>
 */



package ua.com.vhsoft.legis.domain.user.converter;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import ua.com.vhsoft.legis.domain.user.User;
import ua.com.vhsoft.legis.domain.user.dto.UserDto;

@Component
public class UserToDtoConverter implements Converter<User, UserDto> {

    @Override
    public UserDto convert(User source) {
        UserDto dto = new UserDto();

        dto.setId(source.getId());
        dto.setUsername(source.getUsername());
        dto.setFirstName(source.getFirstName());
        dto.setLastName(source.getLastName());
        dto.setMiddleName(source.getMiddleName());

        return dto;
    }

}
