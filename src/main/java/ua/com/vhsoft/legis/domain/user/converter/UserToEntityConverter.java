/*
 * Copyright (C) VHSoft, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Vasyl Hoshovskyi <vasyl.hoshovskyi@vhsoft.com.ua>
 */



package ua.com.vhsoft.legis.domain.user.converter;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import ua.com.vhsoft.legis.domain.user.User;
import ua.com.vhsoft.legis.domain.user.dto.UserDto;

@Component
public class UserToEntityConverter implements Converter<UserDto, User> {

    @Override
    public User convert(UserDto source) {
        User entity = new User();

        entity.setId(source.getId());
        entity.setUsername(source.getUsername());
        entity.setFirstName(source.getFirstName());
        entity.setLastName(source.getLastName());
        entity.setMiddleName(source.getMiddleName());

        return entity;
    }

}
