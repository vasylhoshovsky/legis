/*
 * Copyright (C) VHSoft, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Vasyl Hoshovskyi <vasyl.hoshovskyi@vhsoft.com.ua>
 */


package ua.com.vhsoft.legis.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.password.PasswordEncoder;
import ua.com.vhsoft.legis.security.CustomPasswordEncoder;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Override
    public void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests().antMatchers("/resources/**", "/login*", "/favicon.ico").permitAll();
        http.authorizeRequests()
                .anyRequest().permitAll()
                .and().formLogin()
                .loginPage("/login").permitAll()
                .loginProcessingUrl("/j_spring_security_check")
                .defaultSuccessUrl("/", true)
                .usernameParameter("j_username")
                .passwordParameter("j_password")
                .failureUrl("/login?status=error")
                .and().logout().logoutUrl("/logout")
                .logoutSuccessUrl("/login?status=logout")
                .deleteCookies("JSESSIONID")
                .and().csrf().disable();
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new CustomPasswordEncoder();
    }
}
