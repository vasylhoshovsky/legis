/*
 * Copyright (C) VHSoft, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Vasyl Hoshovskyi <vasyl.hoshovskyi@vhsoft.com.ua>
 *
 */

package ua.com.vhsoft.legis;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @Author Vasyl Hoshovskyi <vasyl.hoshovskyi at vhsoft.com.ua>
 */

@SpringBootApplication
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

}
