/**
 * @Author Vasyl Hoshovskyi <vasyl.hoshovskyi at vhsoft.com.ua>
 */
module ua.com.vhsoft.legis {
    requires java.sql;
    requires java.xml;
    requires java.activation;
    requires validation.api;

    requires hibernate.core;
    requires hibernate.jpa;
    requires hibernate.entitymanager;
    requires hibernate.validator;

    requires spring.core;
    requires spring.beans;
    requires spring.context;
    requires spring.aop;
    requires spring.web;
    requires spring.webmvc;
    requires spring.data.commons;
    requires spring.data.jpa;
    requires spring.tx;
    requires spring.orm;
    requires spring.expression;

    requires spring.security.core;
    requires spring.security.config;
    requires spring.security.web;

    requires spring.boot;
    requires spring.boot.autoconfigure;
    requires spring.boot.starter;

    exports ua.com.vhsoft.legis;
}