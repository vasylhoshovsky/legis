/*
 * Copyright (C) VHSoft, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Vasyl Hoshovskyi <vasyl.hoshovskyi@vhsoft.com.ua>
 */

import { createReducer } from 'redux-act';
import * as HeaderActions from '../actions/HeaderActions';

const INITIAL_STATE = {};

export default createReducer({
    [HeaderActions.testAction]: (state)=> state
}, INITIAL_STATE);