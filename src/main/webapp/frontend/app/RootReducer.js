/*
 * Copyright (C) VHSoft, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Vasyl Hoshovskyi <vasyl.hoshovskyi@vhsoft.com.ua>
 */

import { combineReducers } from 'redux';
import HeaderReducer from './header/reducers/HeaderReducer'

export default combineReducers({
    headerData: HeaderReducer
});